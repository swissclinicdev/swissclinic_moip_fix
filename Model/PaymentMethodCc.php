<?php

namespace Swissclinic\MoipFix\Model;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Payment\Model\InfoInterface;

class PaymentMethodCc extends \Moip\Magento2\Model\PaymentMethodCc {


    public function acceptPayment(InfoInterface $payment)
    {
        parent::acceptPayment($payment);
        return true;
    }

    public function assignData(\Magento\Framework\DataObject $data)
    {
        \Magento\Payment\Model\Method\Cc::assignData($data);
        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);

        if (!is_array($additionalData)) {
            return $this;
        }
        foreach ($additionalData as $key => $value) {
            // Skip extension attributes
            if ($key === \Magento\Framework\Api\ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY) {
                continue;
            }
            $this->getInfoInstance()->setAdditionalInformation($key, $value);
        }
        return $this;
    }

}
